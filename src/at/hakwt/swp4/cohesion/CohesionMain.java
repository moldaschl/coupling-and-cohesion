package at.hakwt.swp4.cohesion;

public class CohesionMain {

    public static void main(String[] args) {
        Car car = new Car();
        Traveler traveler = new Traveler(car);
        Garage garage = new Garage();

        garage.maintainCar(traveler.getCar());

        traveler.startJourney();

        Restaurant restaurant = new Restaurant();
        Meal meal = restaurant.cook();
        traveler.eat(meal);
    }

}
