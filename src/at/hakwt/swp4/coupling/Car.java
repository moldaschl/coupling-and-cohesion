package at.hakwt.swp4.coupling;

public class Car extends Vehicle {

    @Override
    public void move() {
        System.out.println("Car is moving");
    }

}
